*This page describes the video editing ecosystem around Kdenlive. It is not limited to projects which Kdenlive is using.*

## Libraries

| Name | Usage | Description |
| --- | --- | --- |
| MLT | yes | Renders videos |
| FFmpeg | yes | Codecs, effects supported by MLT + called for transcoding |
| frei0r | yes | Video effect library supported by MLT |
| LADSPA | yes | Audio effect library supported by MLT |


## Formats

| Name | Team | Description |
| --- | --- | --- |
| [OpenTimelineIO](http://opentimeline.io/) | Vincent | Import/export FinalCut, Première, AAF, EDL...


## Other video editors and tools

| Name | Description |
| --- | --- |
| [Shotcut](https://www.shotcut.org/) | Libre video editor, multi-platform, by MLT author |
| [Flowblade](http://jliljebl.github.io/flowblade/) | Libre video editor, also based on MLT |
| [Pitivi](http://pitivi.org/) | Libre video editor, based on GStreamer |
| [OpenShot](https://www.openshot.org/) | Libre video editor, v1 based on MLT, v2 on rewritten engine |
| [Olive](https://www.olivevideoeditor.org/) | Libre video editor, young project with new engine |
| [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/) | Paid and gratis version available, multi-platform, professional area (resource intensive) |
| [Ardour](http://ardour.org/) | Digital Audio Workstation |
| [Audacity](https://www.audacityteam.org/) | Audio Editor |